package ca.cegepdrummond;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Test_Serie3_OperateursEtConditions extends SimulConsole {
    @Test
    @Order(1)
    void test_operateur1() throws Exception {
        choixMenu("3a");
        assertSortie("choix 1", false);

    }

    @Test
    @Order(2)
    void test_operateur2() throws Exception {
        choixMenu("3b");
        assertSortie("Réponse A", false);

    }

    @Test
    @Order(3)
    void test_operateur3() throws Exception {
        choixMenu("3c");
        assertSortie("Réponse B", false);

    }
    @Test
    @Order(4)
    void test_operateur4() throws Exception {
        choixMenu("3d");
        assertSortie("Réponse B", false);

    }
    @Test
    @Order(5)
    void test_operateur5() throws Exception {
        choixMenu("3e");
        assertSortie("Réponse A", false);

    }

    @Test
    @Order(6)
    void test_operateur6() throws Exception {
        choixMenu("3f");
        assertSortie("La somme est 9", false);

    }

    @Test
    @Order(7)
    void test_condition1() throws Exception {
        choixMenu("3g");
        assertSortie("bravo", false);
        
    }
    
    @Test
    @Order(8)
    void test_condition2() throws Exception {
        choixMenu("3h");
        assertSortie("choix 2", false);
        
    }
    
    @Test
    @Order(9)
    void test_condition3() throws Exception {
        choixMenu("3i");
        assertSortie("choix 3", false);
        
    }
    
    
    
}
